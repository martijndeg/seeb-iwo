<?php
    
    namespace app;

    use app\core\Core;

    session_start();
    
    require dirname(__DIR__).'/app/core/Core.php';
//    require 'LoadCore.php';

    $app = new Core();
    
    define('CLIENT_LOCATION', $_SERVER['REMOTE_ADDR']);
    
    /*
    *---------------------------------------------------------------
    * ERROR REPORTING
    *---------------------------------------------------------------
    *
    * Different environments will require different levels of error reporting.
    * By default development will show errors but testing and live will hide them.
    */
    if(defined('CLIENT_LOCATION'))
    {
        switch (CLIENT_LOCATION)
        {
            case 'http://localhost':
            case 'http://127.0.0.0':
            case 'https://localhost':
            case 'https://127.0.0.0':
            case '::1':
                error_reporting(E_ALL);
                break;
            default:
                error_reporting(0);
                break;
        }
    }
    
    /**
     *Run checks for server path
     */
    if (isset($_SERVER['PATH_INFO']))
    {
        /**
         *Set var $path to Predefined $_SERVER['PATH_INFO']
         */
        $path = $_SERVER['PATH_INFO'];
        /**
         *Split result or Sever path request into Array
         *@return $path;
         */
        $server = $app->path_split($path);
    }
    /*
    Assign default '/' if previous check result to False
    */
    else
    {
        /**
         *Set path to '/'
         */
        $server = '/';
    }

    switch (Core::is_slash($server))
    {
        case true:
//            require_once dirname(__DIR__).'/app/models/IndexModel.php';
            require_once dirname(__DIR__) . '/app/controllers/IndexController.php';

//            $app->req_model = new IndexModel();
            $app->req_controller = new IndexController($app->req_model);

            /**
             *Model and Controller assignment with first letter as UPPERCASE
             *@return Class;
             */
            $model = $app->req_model;
            $controller = $app->req_controller;

            /**
             *Creating an Instance of the the model and the controller each
             *@return Object;
             */
//            $ModelObj = new $model;
            $ControllerObj = new $controller($app->req_model);

            /**
             *Check if Controller Exist is not empty, then performs an
             *action on the method;
             *@return true;
             */
            if (!empty($app->req_method))
            {
                /**
                 *Assigning Object of Class Init to a Variable, to make it Usable
                 *@return Method Name;
                 */
                $method = $app->req_method;

                /**
                 *Outputs The Required controller and the req *method respectively
                 *@return Required Method;
                 */
                print $ControllerObj->$method($app->req_param);
            }
            else
            {
                /**
                 *This works in only when the Url doesnt have a parameter
                 *@return void;
                 */
                print $ControllerObj->index();
            }
            break;
        case false:

            /**
             *Set Required Controller name ;
             *@return controller;
             *
             */
            $app->req_controller = $server[1];

            /**
             *Set Required Model name
             *@return model;
             */
            $app->req_model = $server[1];

            /**
             *Set Required Method name
             *@return method;
             */
            $app->req_method = isset($server[2])? $server[2] :'';

            /**
             *Set Required Params
             *@return params;
             */
            $app->req_param = array_slice($server, 3);

            /**
             *Check if Controller Exist
             *@return void;
             */
            $req_controller_exists = dirname(__DIR__).'/app/controllers/'.ucfirst($app->req_controller).'Controller.php';
            
            if (file_exists($req_controller_exists))
            {
                /**
                 *Requiring all the files needed i.e The Corresponding Model and Controller
                 *@return corresponding class respectively;
                 */

//                die(dirname(__DIR__).'/app/models/'.ucfirst($app->req_model).'.php');

//                require_once dirname(__DIR__).'/app/models/'.$app->req_model.'.php';
                require_once dirname(__DIR__).'/app/controllers/'.ucfirst($app->req_controller).'Controller.php';

                /**
                 *Model and Controller assignment with first letter as UPPERCASE
                 *@return Class;
                 */
//                $model = ucfirst($app->req_model);

                $controller = ucfirst($app->req_controller).'Controller';
                
                /**
                 *Creating an Instance of the the model and the controller each
                 *@return Object;
                 */
//                $ModelObj = new $model;
                $class_string = "\\app\\controllers\\" .$controller;
                
                $ControllerObj = new $class_string;

                /**
                 *Assigning Object of Class Init to a Variable, to make it Usable
                 *@return Method Name;
                 */
                $method = $app->req_method;
                
//                var_dump($app->req_param[0]);

                /**
                 *Check if Controller Exist is not empty, then performs an
                 *action on the method;
                 *@return true;
                 */
                if ($app->req_method != '')
                {
                    /**
                     *Outputs The Required controller and the req *method respectively
                     *@return Required Method;
                     */
                    print $ControllerObj->$method($app->req_param[0]);
                }
                else
                {
                    /**
                     *This works in only when the Url doesnt have a parameter
                     *@return void;
                     */
                    print $ControllerObj->index();
                }
            }
            else
            {
                header('HTTP/1.1 404 Not Found');
                die('404 - The file - '.$app->req_controller.' - not found');
                //require the 404 controller and initiate it
                //Display its view
            }
            break;
        default:
            print 'An Error Occured';
            break;
    }