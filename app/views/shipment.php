<?php include_once 'partials/header.php'; ?>

    <main>
        <div class="banner-small">
            <img src="/images/citizen_banner-1024x455.jpg" alt="banner"/>
            <div class="banner-small-info">
                <h2>Thanks for your purchase!</h2>
                <a href="pages/index.php">Home</a> <strong>/ Shipment</strong>
            </div>
        </div>
        <div class="container">
            <div class="shipment">
                <h3>Thank you for shopping at Captain Watch</h3>
                <h4>Your watch will be shipped to the following address</h4>
                <div class="shipping-address">
                    <ul>
                        <li>
                            <strong>Customer's name:</strong> <?= $user->name($_SESSION['uid']) ?>
                        </li>
                        <li>
                            <strong>Address:</strong> <?= $user->address($_SESSION['uid']) ?>
                        </li>
                        <li>
                            <strong>Postal Code:</strong> <?= $user->postalcode($_SESSION['uid']) ?>
                        </li>
                        <li>
                            <strong>City:</strong> <?= $user->city($_SESSION['uid']) ?>
                        </li>
                        <li>
                            <strong>Email:</strong> <?= $user->email($_SESSION['uid']) ?>
                        </li>
                    </ul>
                </div>
                <a class="button" href="/"><span class="material-icons">home</span> Home</a>
            </div>
        </div>
    </main>
<?php include_once 'partials/footer.php';