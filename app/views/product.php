<?php include_once 'partials/header.php'; ?>
    
    <main>
        <div class="banner-small">
            <img src="/images/citizen_banner-1024x455.jpg" alt="banner"/>
            <div class="banner-small-info">
                <h2><?= $product->name ?></h2>
                <a href="/">Home</a><strong> / <?= $product->name ?></strong>
            </div>
        </div>
        <div class="container">
            <div class="product-page-margin">
                <div class="product-left-side">
                    <img src="<?= !empty($product->image_path) ? $product->image_path : 'https://i.pinimg.com/originals/5d/d8/e9/5dd8e91efcc5b625433901d4f78fac36.jpg' ?>" alt="<?= $product->brand.' '.$product->name ?>">
                </div>
                <div class="product-right-side">
                    <h2><?= $product->name ?></h2>
                    <h3><?= $product->brand ?></h3>
                    <h4 class="product-price">&euro; <?= display_price($product->price) ?></h4>
                    <table id="product_info">
                        <tr><td class="detail">Stock</td><td class="value"><?= $product->stock ?></td></tr>
                        <tr><td class="detail">Color</td><td class="value"><?= $product->color ?></td></tr>
                        <tr><td class="detail">Style</td><td class="value"><?= $product->category ?></td></tr>
                    </table>
                    <p><?= $product->description ?></p>
                    <form method="post" action="/cart/add/<?= $product->id ?>">
                        <div class="product-qty">
                            <input type="number" min="1" max="<?= $product->stock ?>" step="1" name="quantity" value="1" autocomplete="off">
                        </div>
                        <button class="button"><span class="material-icons">shopping_cart</span><span class="fancy-button"> Order</span></button>
                    </form>
                </div>
            </div>
            <div class="divider">
                <br>
            </div>
            <div class="related-products">
                <h3>Our Top Picks</h3>
                <div class="shop-flex">
                    <?php foreach ($related as $product) :?>
    
                        <?php include 'partials/product-overview.php'?>
                    
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </main>

<?php include_once 'partials/footer.php';