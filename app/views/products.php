<?php include_once 'partials/header.php'; ?>

<main>
    <div class="banner-small">
        <img src="/images/citizen_banner-1024x455.jpg" alt="banner"/>
        <div class="banner-small-info">
            <h2>Shop</h2>
            <a href="<?php echo URLROOT; ?>pages/index.php">Home</a><strong>/ Shop</strong>
        </div>
    </div>
    <div class="container">
        <div class="shop-left-side">
            <div class="shop-menu">
                <h3>Brands</h3>
                <ul>
                    <?php foreach($brands as $brand) { ?>
                        
                        <li><a href="/product/brand/<?= $brand->id ?>"><?= $brand->name ?></a></li>
                        
                    <?php } ?>
                </ul>
            </div>
            <div class="shop-menu">
                <h3>Styles</h3>
                <h5>Hers</h5>
                <ul>
                    <?php foreach($categories as $category) { ?>
                        <?php if($category->gender == 1) { ?>
                        
                            <li><a href="/product/category/<?= $category->id ?>"><?= $category->name ?></a></li>
                            
                        <?php } ?>
                    <?php } ?>
                </ul>
                <h5>His</h5>
                <ul>
                    <?php foreach($categories as $category) { ?>
                        <?php if($category->gender == 2) { ?>
                            
                            <li><a href="/product/category/<?= $category->id ?>"><?= $category->name ?></a></li>
        
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="advert">

            </div>
            <div class="shop-menu">
                <h3>Best sellers</h3>
                <?php $i = 0 ?>
                <?php foreach($bestsellers as $product) :?>
                    <div class="product-overview">
                        <div class="product-overview-left">
                            <img src="<?= !empty($product->image_path) ? $product->image_path : 'https://i.pinimg.com/originals/5d/d8/e9/5dd8e91efcc5b625433901d4f78fac36.jpg' ?>" alt="<?= $product->brand.' '.$product->name ?>"/>
                        </div>
                        <div class="product-overview-right">
                            <?= $product->name ?>
                            <h4>&euro; <?= display_price($product->price) ?></h4>
                            <form method="post" action="/cart/add/<?= $product->id ?>">
                                <input type="hidden" name="quantity" value="1">
                                <button><span class="material-icons">shopping_cart</span></button>
                            </form>
                        </div>
                    </div>
                    <?php if(++$i == 3) break;?>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="shop-products">
            <div class="shop-nav"></div>
            <div class="shop-flex">
                <?php foreach($products as $product) { ?>

                    <?php include 'partials/product-overview.php'?>

                <?php } ?>
            </div>
        </div>
    </div>
</main>

<?php include_once 'partials/footer.php'; ?>


