<?php include_once 'partials/header.php'; ?>
    
    <main>
        <div class="banner">
            <div>
                <img src="/images/header-image.jpg" alt="banner">
                <div class="banner-clickables">
                    <h2>My Account</h2>
                    <h3>Easy order and payment</h3>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-3-3">
                <h2>My Account</h2>
                <p>This is your personal account page. Here you can find all your details and update your information.</p>
            </div>
            <div class="col-3-6">
                <form method="post" action="/user/changepassword">
                    <p>Change your password</p>
                    <div class="col-1-3 inline-label">
                        <label for="old_password">Old Password</label>
                    </div>
                    <div class="col-2-3">
                        <input type="password" class="input-user" name="old_password" id="old_password" required placeholder="Old password">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="password1">New Password</label>
                    </div>
                    <div class="col-2-3">
                        <input type="password" class="input-user" name="password1" id="password1" required placeholder="New password">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="password2">Retype New Password</label>
                    </div>
                    <div class="col-2-3">
                        <input type="password" class="input-user" name="password2" id="password2" required placeholder="New password">
                    </div>
                    <div class="col-3-3">
                        <button type="submit"><span class="material-icons">check</span> Change password</button>
                    </div>
                </form>
                <div class="form-spacer"></div>
            </div>
            <div class="col-3-6">
                <form method="post" action="/user/update">
                    <p>Change your shipping information or email address.</p>
                    <div class="col-1-3 inline-label">
                        <label for="email_reg">Email</label>
                    </div>
                    <div class="col-2-3">
                        <input type="email" class="input-user" name="email" autocomplete="email" required id="email_reg" placeholder="Your email address" value="<?= $current_user->email ?>">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="address">Address</label>
                    </div>
                    <div class="col-2-3">
                        <input type="text" class="input-user" name="address" id="address" autocomplete="address-line1" required placeholder="Highstreet 1" value="<?= $current_user->address ?>">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="zipcode">Zipcode</label>
                    </div>
                    <div class="col-2-3">
                        <input type="text" class="input-user" name="zipcode" id="zipcode" autocomplete="postal-code" required placeholder="123456" value="<?= $current_user->zipcode ?>">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="city">City/Town</label>
                    </div>
                    <div class="col-2-3">
                        <input type="text" class="input-user" name="city" id="city" autocomplete="address-level2" required placeholder="Fairy Town" value="<?= $current_user->city ?>">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="country">Country</label>
                    </div>
                    <div class="col-2-3">
                        <select class="input-user" name="country" id="country" autocomplete="country" required>
                            <option value="">Select your Country</option>
                            <?php foreach($countries as $country) { ?>
                                <option value="<?= $country->alpha3Code ?>" <?= ($current_user->country == $country->alpha3Code)? 'selected="selected"' : '' ?>><?= $country->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-3-3">
                        <button type="submit"><span class="material-icons">how_to_reg</span> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </main>


<?php include_once 'partials/footer.php'; ?>