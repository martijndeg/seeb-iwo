<div class="product">
    <a href="/product/show/<?= $product->id ?>">
        <img src="<?= !empty($product->image_path) ? $product->image_path : 'https://i.pinimg.com/originals/5d/d8/e9/5dd8e91efcc5b625433901d4f78fac36.jpg' ?>" alt="<?= $product->brand.' '.$product->name ?>"/>
        <h3 class="product-name"><?= $product->brand ?></h3>
        <span class="brand-name"><?= $product->name ?></span>
        <div class="price-container">&euro; <?= display_price($product->price) ?></div>
    </a>
</div>