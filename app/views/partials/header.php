<!doctype html>
<html class="no-js" lang="nl">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?= $page['title_prefix']?> | <?= $title ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> <!-- deze tag zorgt ervoor dat de browser weet dat je een responsive website wilt laten zien die geen rare zoom dingen doet in formulieren e.d. -->
    
        <link rel="manifest" href="site.webmanifest"> <!-- deze en de volgende tag zijn voor de favicon (dat kleine icoontje in je browser) en voor de icoontjes op het home screen van je device -->
        <link rel="apple-touch-icon" href="icon.png">
        <!-- Place favicon.ico in the root directory -->
    
    
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dawning+of+a+New+Day" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/main.css">
        <!-- volgorde stylesheets: zoals in een stylesheet worden de stylesheets zelf ook volgorde geladen en de laatste heeft dus altijd voorrang, daar zet je dus altijd je eigen stylesheet zodat je alles kunt overriden. Het je meerdere eigen stylesheets, dan zet je ze op specificiteit  -->
    </head>
    
    <body>
    <!--[if lte IE 9]
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->
    <header>
        <div class="container">
            <div class="header-divider">
                &nbsp;
            </div>
            <div class="header-divider">
                <div class="header-logo">
                    <h1><img src="/images/logo.png" alt="Captain Watch"/></h1>
                </div>
            </div>
            <div class="header-divider">
            </div>
        </div>
        <?php include_once 'nav.php'; ?>
    </header>
