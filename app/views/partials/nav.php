<nav>
    <div class="navbar">
        <div class="navbar-left">
            <a href="/"><span class="material-icons">home</span><span class="hidden-mobile"> Home</span></a>
            <div class="dropdown">
                <button class="dropdown-btn"><span class="material-icons">watch_later</span><span class="hidden-mobile"> Our Collection</span>
                    <i class="fa fa-caret-down"></i>
                </button>
                <div class="dropdown-content">
                    <div class="row">
                        <div class="col-1-3">
                            <div class="col-1-6">
                                <div class="gender-name">Women</div>
                            </div>
                            <div class="col-5-6">
                                <?php foreach($menu as $category) { ?>
                                    <?php if($category->gender == 1) { ?>
                                        <a href="/product/category/<?= $category->id ?>"><?= $category->name ?></a>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-1-3">
                            <div class="col-1-6">
                                <div class="gender-name">Men</div>
                            </div>
                            <div class="col-5-6">
                                <?php foreach($menu as $category) { ?>
                                    <?php if($category->gender == 2) { ?>
                                        <a href="/product/category/<?= $category->id ?>"><?= $category->name ?></a>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-1-3" id="menu_branding">
                            <div id="menu_branding_image">
                                <img src="/images/edox-960x580.jpg" alt="Edox">
                            </div>
                            <div id="menu_branding_logo">
                                <img src="/images/logo.png" alt="Logo">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="/page/show/1"><span class="material-icons">people_outline</span><span class="hidden-mobile"> About us</span></a>
        </div>
        <div class="navbar-right">
            <a href="<?= $user->is_loggedin() ? '/user/account' : '/user/register' ?>"><span class="material-icons">person</span><?= $user->is_loggedin() ? ' '.$user->name($_SESSION['uid']): '' ?></a>
            <a href="/cart" id="cart_menu"><span class="material-icons">shopping_cart</span><?= (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) ? '<span id="cart_item_no">'.count($_SESSION['cart']).'</span>' : ''; ?></a>
            <?= $user->is_loggedin() ? '<a href="/user/logout"><span class="material-icons">lock_open</span></a>' : '' ?>
        </div>
    </div>
</nav>