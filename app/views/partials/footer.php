        <footer>
            <div class="footer-box">
                <div class="container">
                    <div class="footer-flex">
                        <div class="footer-wrap">
                            <h3>Contact</h3>
                            <ul>
                                <li>
                                    <div class="address-icon-container">
                                        <span class="material-icons">location_on</span>
                                    </div>
                                    <div class="address-container">
                                        352 Stardust Lane<br>
                                        J78H90 Fairy Town
                                    </div>
                                </li>
                                <li><span class="material-icons">alternate_email</span> <a href="mailto:email@captainwatch.nl">email@captainwatch.nl</a></li>
                            </ul>
                        </div>
                        <div class="footer-wrap">
                            <h3>Products</h3>
                            <ul>
                                <li>
                                    <a href="/product">Men</a>
                                </li>
                                <li>
                                    <a href="/product">Women</a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-wrap">
                            <h3>Need help</h3>
                            <ul>
                                <li>
                                    <a href="#">Delivery information</a>
                                </li>
                                <li>
                                    <a href="#">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="#">Terms & Conditions</a>
                                </li>
                                <li>
                                    <a href="/page/show/1">Contact</a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-wrap">
                            <h3>Need help</h3>
                            <ul>
                                <li>
                                    <a href="#">Delivery information</a>
                                </li>
                                <li>
                                    <a href="#">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="#">Terms & Conditions</a>
                                </li>
                                <li>
                                    <a href="/page/show/1">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="copyrights">
                        &copy; <?= date('Y') ?> All Rights Reserved. Design By Super Cool Dudes
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>