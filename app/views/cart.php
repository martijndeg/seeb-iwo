<?php include_once 'partials/header.php'; ?>

    <main>
        <div class="banner-small">
                <img src="/images/citizen_banner-1024x455.jpg" alt="banner"/>
            <div class="banner-small-info">
                <h2>Shopping Cart</h2>
                <a href="/">Home</a><strong>/ Shopping Cart</strong>
            </div>
        </div>
        <div class="container">
            <div class="cart-bg">
                <div class="cart-header">
                    <div class="flex-1">

                    </div>
                    <div class="flex-2">
                        <h4>Name</h4>
                    </div>
                    <div class="flex-1">
                        <h4>Price</h4>
                    </div>
                    <div class="flex-1">
                        <h4>Quantity</h4>
                    </div>
                    <div class="flex-1">
                        <h4>Subtotal</h4>
                    </div>
                    <div class="flex-1">
                        <h4>Remove</h4>
                    </div>
                </div>
                <?php foreach($products as $product) : ?>
                    <div class="cart-item">
                        <div class="cart-item-img flex-1">
                            <a href="/product/show/<?= $product->id ?>"><img src="<?= !empty($product->image_path) ? $product->image_path : 'https://i.pinimg.com/originals/5d/d8/e9/5dd8e91efcc5b625433901d4f78fac36.jpg' ?>" alt="<?php echo $product->name ?>"/></a>
                        </div>
                        <div class="cart-item-info flex-2">
                            <p><a href="/product/show/<?= $product->id ?>"><?php echo $product->name ?></a></p>
                        </div>
                        <div class="cart-item-info flex-1">
                            <h4>&euro; <?= display_price($product->price) ?></h4>
                        </div>
                        <div class="cart-item-info flex-1">
                            <form method="post" action="/cart/update">
                                <div class="product-qty">
                                    <input type="hidden" name="product[]" value="<?= $product->id ?>">
                                    <input type="number" min="0" max="<?= $product->stock ?>" step="1" name="quantity[]" value="<?= $product->quantity ?>" autocomplete="off" onchange="this.form.submit()">
                                </div>
                            </form>
                        </div>
                        <div class="cart-item-info flex-1">
                            <h4>&euro; <?= display_price($product->price * $product->quantity) ?></h4>
                        </div>
                        <div class="cart-item-info-delete flex-1">
                            <a href="/cart/delete/<?= $product->id ?>"><span class="material-icons">delete_forever</span></a>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="cart-ks-button">
                    <a class="button" href="/product"><span class="material-icons">chevron_left</span> Keep Shopping</a>
                </div>
                <div class="divider">
                    <br>
                </div>
            </div>
            <div class="cart-bottom">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,
                    ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
            </div>
            <div class="cart-bottom">
                <div class="total-bg">
                    <div class="cart-total">
                        <div class="cart-total-info flex-1">
                            <p>Item<?= (count($products) > 1) ? 's' : ''?> Subtotal</p>
                        </div>
                        <div class="cart-total-info flex-1">
                            <h4>&euro; <?= display_price($subtotal) ?></h4>
                        </div>
                    </div>
                    <div class="cart-total">
                        <div class="cart-total-info flex-1">
                            <p>Shipping</p>
                        </div>
                        <div class="cart-total-info flex-1">
                            <h4>&euro; <?= display_price($shipment) ?></h4>
                        </div>
                    </div>
                    <div class="cart-total">
                        <div class="cart-total-info flex-1">
                            <h4>Total Amount</h4>
                        </div>
                        <div class="cart-total-info flex-1">
                            <h4>&euro; <?= display_price($subtotal + $shipment) ?></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cart-checkout-button">
                <a class="button" href="<?= ($user->is_loggedin()) ? '/cart/checkout' : '/user/register' ?>"><span class="material-icons">payment</span> Checkout</a>
            </div>
        </div>
    </main>

<?php include_once 'partials/footer.php';