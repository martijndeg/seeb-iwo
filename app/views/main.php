<?php include_once 'partials/header.php'; ?>

    <main>
        <div class="banner">
            <div>
                <img src="/images/header-image.jpg" alt="banner">
                <div class="banner-clickables">
                    <h2>the new collection</h2>
                    <h3>luxurious watches for you</h3>
                    <a class="button" href="/product"><span class="material-icons">shopping_cart</span><div class="fancy-button"> Shop now</div></a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="call-to-action">
                <div class="call-to-action-box">
                    <img src="/uploads/products/ancon-military.jpg" alt="Ancon Military">
                    <div class="call-to-action-clickables">
                        <h3>Steel</h3>
                        <a class="button" href="/product/category/3"><span class="material-icons">shopping_cart</span><div class="fancy-button"> Shop now</div></a>
                    </div>
                </div>
                <div class="call-to-action-box">
                    <img src="/uploads/products/iwc-worldtimer.jpg" alt="IWC World Timer"/>
                    <div class="call-to-action-clickables">
                        <h3>Leather</h3>
                        <a class="button" href="/product/category/4"><span class="material-icons">shopping_cart</span><div class="fancy-button"> Shop now</div></a>
                    </div>
                </div>
                <div class="call-to-action-box">
                    <img src="/uploads/products/rolex-serpico.jpg" alt="Rolex Serpico"/>
                    <div class="call-to-action-clickables">
                        <h3>Digital</h3>
                        <a class="button" href="/product/category/5"><span class="material-icons">shopping_cart</span><div class="fancy-button"> Shop now</div></a>
                    </div>
                </div>
            </div> <!--end call to action div-->
            <div class="product-section">
                <h1>Our favorite watches</h1>
                <div class="divider"></div>
                <div class="products">
                    
                    <?php foreach($products as $product) { ?>
    
                        <?php include 'partials/product-overview.php'?>
    
                    <?php } ?>
                </div>  <!--end products div-->
            </div>    <!--end product section div-->
        </div>   <!--end container div-->
        <div class="testimonials">
            <div class="container">
                
                <div class="testimonial">
                    <div class="testimonial-user">
                        <img src="https://timedotcom.files.wordpress.com/2017/08/shonda-rhimes_luisa-dorr-time-firsts-20171.jpg?quality=85&w=600&h=600&crop=1&zoom=2" alt="Jane Doe"/>
                    </div>
                    <div class="testimonial-content">
                        <strong class="testimonial-tagline">Best Selection</strong>
                        <p class="testimonial-quote">I love the new selections every month and the friendly people.</p>
                        <span class="testimonial-name">- Jane Doe -</span>
                    </div>
                </div>
                <div class="testimonial">
                    <div class="testimonial-user">
                        <img src="/images/mitchel.jpg" alt="Mitchel Abrahams"/>
                    </div>
                    <div class="testimonial-content">
                        <strong class="testimonial-tagline">Great service</strong>
                        <p class="testimonial-quote">Great services, I received my order within 3 days after ordering.</p>
                        <span class="testimonial-name">- Mitchel Abrahams -</span>
                    </div>
                </div>
                <div class="testimonial">
                    <div class="testimonial-user">
                        <img src="https://41dcdfcd4dea0e5aba20-931851ca4d0d7cdafe33022cf8264a37.ssl.cf1.rackcdn.com/7958484_transgender-model-andreja-pejic-lands-her_9f4716c2_m.jpg?bg=857D7B" alt="Lana Smith"/>
                    </div>
                    <div class="testimonial-content">
                        <strong class="testimonial-tagline">Fast delivery</strong>
                        <p class="testimonial-quote">I always use the overnight delivery option. It never fails.</p>
                        <span class="testimonial-name">- Lana Smith -</span>
                    </div>
                </div>
                
            </div>
        </div>  <!--end testimonials div-->
        <div class="container">
            <h1>The best deals</h1>
            <div class="divider"></div>
            <div class="sale-block">
                <div class="sale-items">
                    
                    <?php
                        $i = 1;
                        foreach($deals as $product) {
                    ?>
                    
                        <div class="sale-item <?= ($i % 4 == 0) ? 'flex-2' : 'flex-1'?>">
                            <img src="<?= !empty($product->image_path) ? $product->image_path : 'https://i.pinimg.com/originals/5d/d8/e9/5dd8e91efcc5b625433901d4f78fac36.jpg' ?>" alt="<?= $product->brand.' '.$product->name ?>"/>
                            <div class="sale-item-info">
                                <div class="sale-item-text-container">
                                    <h4 class="sale-item-title"><?= $product->name ?></h4>
                                    <h5 class="sale-item-subtitle"><?= $product->brand ?></h5>
                                    <a href="/product/show/<?= $product->id ?>"><span class="material-icons">info</span></a>
                                </div>
                            </div>
                        </div>
                        
                        <?php if($i % 3 == 0) { ?>
                    
                            </div>
                            <div class="sale-items">
                    
                        <?php } ?>
                    
                    <?php
                            $i++;
                        }
                    ?>
                    
                </div>
            </div>  <!--end sale-block div-->
        </div>    <!--end container div-->
    </main>
 
<?php include_once 'partials/footer.php';