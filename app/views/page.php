<?php include_once 'partials/header.php'; ?>
    
    <main>
        <div class="banner-small">
            <img src="/images/citizen_banner-1024x455.jpg" alt="banner"/>
            <div class="banner-small-info">
                <h2><?= $page_title ?></h2>
                <a href="/">Home</a><strong> / <?= $page_title ?></strong>
            </div>
        </div>
        <div class="container">
            <div class="product-page-margin">
                <div class="product-left-side">
                    <img src="<?= !empty($image_path) ? $image_path : 'https://i.pinimg.com/originals/5d/d8/e9/5dd8e91efcc5b625433901d4f78fac36.jpg' ?>" alt="<?= $page_title ?>">
                </div>
                <div class="product-right-side">
                    <h2><?= $page_title ?></h2>
                    <?= $content ?>
                </div>
            </div>
        </div>
    </main>

<?php include_once 'partials/footer.php';