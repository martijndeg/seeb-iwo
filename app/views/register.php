<?php include_once 'partials/header.php'; ?>
    
    <main>
        <div class="banner">
            <div>
                <img src="/images/header-image.jpg" alt="banner">
                <div class="banner-clickables">
                    <h2>Register</h2>
                    <h3>Become part of the club</h3>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-3-6">
                <h2>Sign in...</h2>
                <p>Sign in to your personal account and order right away.</p>
                <form method="post" action="/user/signin">
                    <div class="col-1-3 inline-label">
                        <label for="email">Email</label>
                    </div>
                    <div class="col-2-3">
                        <input type="email" class="input-user" name="email" autocomplete="email" required id="email" placeholder="Your email address">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="password">Password</label>
                    </div>
                    <div class="col-2-3">
                        <input type="password" class="input-user" name="password" id="password" required placeholder="Your password">
                    </div>
                    <div class="col-3-3">
                        <button type="submit"><span class="material-icons">check</span> Sign in</button>
                    </div>
                </form>
            </div>
            <div class="col-3-6">
                <h2>...or register now</h2>
                <p>We just need a few details to setup your personal account.</p>
                <form method="post" action="/user/save">
                    <div class="col-1-3 inline-label">
                        <label for="email_reg">Email</label>
                    </div>
                    <div class="col-2-3">
                        <input type="email" class="input-user" name="email" autocomplete="email" required id="email_reg" placeholder="Your email address">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="password1">Password</label>
                    </div>
                    <div class="col-2-3">
                        <input type="password" class="input-user" name="password1" id="password1" required placeholder="Your password">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="password2">Retype Password</label>
                    </div>
                    <div class="col-2-3">
                        <input type="password" class="input-user" name="password2" id="password2" required placeholder="Your password">
                    </div>
                    <div class="form-spacer"></div>
                    <div class="col-1-3 inline-label">
                        <label for="name">Name</label>
                    </div>
                    <div class="col-2-3">
                        <input type="text" class="input-user" name="name" id="name" autocomplete="name" required placeholder="John Doe">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="address">Address</label>
                    </div>
                    <div class="col-2-3">
                        <input type="text" class="input-user" name="address" id="address" autocomplete="address-line1" required placeholder="Highstreet 1">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="zipcode">Zipcode</label>
                    </div>
                    <div class="col-2-3">
                        <input type="text" class="input-user" name="zipcode" id="zipcode" autocomplete="postal-code" required placeholder="123456">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="city">City/Town</label>
                    </div>
                    <div class="col-2-3">
                        <input type="text" class="input-user" name="city" id="city" autocomplete="address-level2" required placeholder="Fairy Town">
                    </div>
                    <div class="col-1-3 inline-label">
                        <label for="country">Country</label>
                    </div>
                    <div class="col-2-3">
                        <select class="input-user" name="country" id="country" autocomplete="country" required>
                            <option value="">Select your Country</option>
                            <?php foreach($countries as $country) { ?>
                                <option value="<?= $country->alpha3Code ?>"><?= $country->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-3-3">
                        <button type="submit"><span class="material-icons">how_to_reg</span> Register</button>
                    </div>
                </form>
            </div>
        </div>
    </main>

<?php include_once 'partials/footer.php';