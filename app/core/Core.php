<?php
    namespace app\core;

    use app\models\Category;
    use app\models\User;

    require_once 'Template.php';
    require_once dirname(__DIR__).'/settings.php';

    class Core {
        /*
        * Class Properties Declaration
        */
        public $environment;
        public $capsule;
        public $controller_path;
        public $req_controller;
        public $req_model;
        public $view_render;
        public $req_param;
        public $req_method;
        public $load;
        private static $global_data = ['title' => TITLE];
        
        
        /**
         *Split server path request into Array
         *@return array $capsule;
         */
        public function path_split($path)
        {
            $this->capsule = explode('/', ltrim($path));
            return $this->capsule;
        }
        
        /**
         *Check is url results to a trailing Slash
         *@return bool;
         */
        public static function is_slash($path)
        {
            /**
             *Is path = '/'
             *@return true;
             */
            if ($path == '/') {
                return true;
            }
            
            /**
             *Is path != '/'
             *@return false;
             */
            else
            {
                return false;
            }
        }
        
        public static function set_page_global_data(array $data)
        {
            $categories = new Category();
            $user = new User();
            
            self::$global_data['menu'] = $categories->getAll();
            self::$global_data['page'] = $data;
            self::$global_data['user'] = $user;
        }
        
        public static function view($template_name, $data = array())
        {
            $loader = new Template();
            $loader->global_vars(self::$global_data);
            $template = $loader->load($template_name.'.php', $data, true);
            
            echo $template;
        }
    }