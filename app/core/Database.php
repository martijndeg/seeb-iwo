<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 13/06/2018
 * Time: 15:00
 */

    namespace app\core;

    class Database
    {
        private $host = DB_HOST;
        private $user = DB_USER;
        private $pass = DB_PASS;
        private $dbname = DB_NAME;
        
        private $dbh;
        private $stmt;
    
        public function __construct()
        {
            //set dsn
            $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
            $options = array(
                \PDO::ATTR_PERSISTENT => true,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            );
            
            try
            {
                $this->dbh = new \PDO($dsn, $this->user, $this->pass,$options);
            }
            catch(PDOException $e)
            {
                echo $e->getMessage();
            }
        }
    
        //prepare statement with query
        public function query($sql)
        {
            $this->stmt = $this->dbh->prepare($sql);
        }
    
        //bind values
        public function bind($param, $value, $type = null)
        {
            if(is_null($type))
            {
                switch(true)
                {
                    case is_int($value);
                        $type = \PDO::PARAM_INT;
                        break;
                    case is_bool($value);
                        $type = \PDO::PARAM_BOOL;
                        break;
                    case is_null($value);
                        $type = \PDO::PARAM_NULL;
                        break;
                    default:
                        $type = \PDO::PARAM_STR;
                }
            }
            $this->stmt->bindValue($param, $value, $type);
        }
    
        //execute the prepared statement
        public function execute()
        {
            return $this->stmt->execute();
        }
    
        //Get result set as array of object
        public function resultSet()
        {
            $this->execute();
            return $this->stmt->fetchAll(\PDO::FETCH_OBJ);
        }
    
        //get single record as object
        public function single()
        {
            $this->execute();
            return $this->stmt->fetch(\PDO::FETCH_OBJ);
        }
    
        //get row count
        public function rowCount()
        {
            return $this->stmt->rowCount();
        }
    }