<?php
    
    namespace app\core;

    class Template
    {
        private $globals = array();
        private $views_folder;
    
        public function __construct(){
            $this->views_folder = dirname(__DIR__).'/views/';
        }
    
        public function global_vars($key, $value = null)
        {
            if(is_array($key))
            {
                foreach($key as $k => $v)
                {
                    $this->global_vars($k, $v);
                }
            }
            else
            {
                $this->globals[$key] = $value;
            }
        }
    
        public function load($tpl, $arr = array(), $return = false)
        {
            if($this->views_folder.file_exists($tpl))
            {
                foreach($arr as $key => $value)
                {
                    $$key = $value;
                }
                
                unset($arr);
    
                foreach($this->globals as $key => $value)
                {
                    $$key = $value;
                }
    
                ob_start();
                require_once($this->views_folder.$tpl);
                
                $template = ob_get_contents();
                ob_end_clean();
    
                if($return == false)
                {
                    echo $template;
                }
                else
                {
                    return $template;
                }
            }
            else
            {
                return false;
            }
        }
        
    }
