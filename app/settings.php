<?php

    namespace app;

    define('TITLE', 'Captain Watch');
    
    define('DB_HOST', 'localhost');
    define('DB_NAME', 'lolhan_capwatch');
    define('DB_USER', 'lolhan_capuser');
    define('DB_PASS', 'Z3pVH5AgA58IpCzh');
    
    define('ABS_PATH', dirname(__DIR__). DIRECTORY_SEPARATOR . 'app');
    
    spl_autoload_register(function ($class) {
        // project-specific namespace prefix
        $prefix = 'app\\';
    
        // does the class use the namespace prefix?
        $len = strlen($prefix);
        if (strncmp($prefix, $class, $len) !== 0) {
            // no, move to the next registered autoloader
            return;
        }
        
        // get the relative class name
        $relative_class = substr($class, $len);
        
        // replace the namespace prefix with the base directory, replace namespace
        // separators with directory separators in the relative class name, append
        // with .php
        $file = ABS_PATH . "/" . str_replace('\\', '/', $relative_class) . '.php';
    
        // if the file exists, require it
        if (file_exists($file)) {
            require $file;
        }
    });
    
    include_once 'helpers/core-helpers.php';
    