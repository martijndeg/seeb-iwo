<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 13/06/2018
 * Time: 14:58
 */
    namespace app\models;

    class Product extends Model
    {
        protected $table = 'products';
    
        public function getAll($orderBy = 'id', $order = 'ASC', $brand_id = null, $limit = 1000, $category_id = null)
        {
            if($category_id == null)
            {
                if($brand_id == null)
                {
                    $this->db->query('SELECT p.*, c.name AS category, b.name AS brand, g.gender AS gender FROM products p LEFT JOIN categories c ON p.category = c.id LEFT JOIN genders g ON c.gender = g.id LEFT JOIN brands b ON p.brand = b.id ORDER BY '.$orderBy.' '.$order.' LIMIT '.$limit);
                }
                else
                {
                    $this->db->query('SELECT p.*, c.name AS category, b.name AS brand, g.gender AS gender FROM products p LEFT JOIN categories c ON p.category = c.id LEFT JOIN genders g ON c.gender = g.id LEFT JOIN brands b ON p.brand = b.id WHERE p.brand = '.$brand_id.' ORDER BY '.$orderBy.' '.$order.' LIMIT '.$limit);
                }
            }
            else
            {
                $this->db->query('SELECT p.*, c.name AS category, b.name AS brand, g.gender AS gender FROM products p LEFT JOIN categories c ON p.category = c.id LEFT JOIN genders g ON c.gender = g.id LEFT JOIN brands b ON p.brand = b.id WHERE p.category = :category ORDER BY '.$orderBy.' '.$order.' LIMIT '.$limit);
                $this->db->bind(':category', $category_id);
            }
            

            $this->db->execute();
        
            return $this->db->resultSet();
        }
        
        public function find($id)
        {
            $this->db->query('SELECT p.*, c.id AS category_id, c.name AS category, b.name AS brand, g.gender AS gender FROM products p LEFT JOIN categories c ON p.category = c.id LEFT JOIN genders g ON c.gender = g.id LEFT JOIN brands b ON p.brand = b.id WHERE p.id = :id');
            $this->db->bind(':id', $id);
            
            $this->db->execute();
            
            return $this->db->single();
        }
        
        public function related($id, $limit = 4)
        {
            $this->db->query('SELECT category FROM products WHERE id = :id');
            $this->db->bind(':id', $id);
            
            $this->db->execute();
            
            $product = $this->db->single();
            $category = $product->category;
            
            $this->db->query('SELECT p.*, c.name AS category, b.name AS brand, g.gender AS gender FROM products p LEFT JOIN categories c ON p.category = c.id LEFT JOIN genders g ON c.gender = g.id LEFT JOIN brands b ON p.brand = b.id WHERE p.category = :category AND p.id != :id ORDER BY p.price DESC LIMIT '.$limit);
            $this->db->bind(':category', $category);
            $this->db->bind(':id', $id);
            
            $this->db->execute();
    
            return $this->db->resultSet();
        }
        
        public function sell($product_data)
        {
            $this->db->query("UPDATE products SET stock = stock - :quantity WHERE id = :id");
            $this->db->bind(':quantity', $product_data['quantity']);
            $this->db->bind(':id', $product_data['id']);
            
            if($this->db->execute())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
