<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 13/06/2018
 * Time: 14:54
 */

    namespace app\models;

    use app\core\Database;

    abstract class Model
    {
        private $name;
        protected $db;
        protected $table;
        protected $attributes = [];
        
        public function __construct()
        {
            $this->db = new Database();
        }
        
        public function find($id)
        {
            $this->db->query('SELECT * FROM '.$this->table.' WHERE id = :id');
            
            $this->db->bind(':id', $id);
            
            return $this->db->single();
        }
        
        public function getAll($orderBy = 'id', $order = 'ASC', $limit = 1000)
        {
            $this->db->query('SELECT * FROM '.$this->table.' ORDER BY '.$orderBy.' '.$order.' LIMIT '.$limit);
            $this->db->bind(':orderby', $orderBy);
            $this->db->bind(':order',  $order);
            
            $this->db->execute();
            
            return $this->db->resultSet();
        }
    }