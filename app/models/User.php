<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 18/06/2018
 * Time: 17:01
 */

namespace app\models;


class User extends Model
{
    protected $table = 'users';
    
    public function save($user_data)
    {
        if($user_data['password1'] == $user_data['password2'])
        {
            $password = password_hash($user_data['password1'], PASSWORD_BCRYPT);
    
            $this->db->query('INSERT INTO users (email, name, password, address, zipcode, city, country) VALUE (:email, :name, :password, :address, :zipcode, :city, :country)');
            $this->db->bind(':email', $user_data['email']);
            $this->db->bind(':name', $user_data['name']);
            $this->db->bind(':password', $password);
            $this->db->bind(':address', $user_data['address']);
            $this->db->bind(':zipcode', $user_data['zipcode']);
            $this->db->bind(':city', $user_data['city']);
            $this->db->bind(':country', $user_data['country']);
            
            if($this->db->execute())
            {
                return true;
            }
            else
            {
                $_SESSION['error'] = 'Please fill out all fields.';
                
                return false;
            }
        }
        else
        {
            $_SESSION['error'] = 'Your passwords do not match.';
            
            return false;
        }
    }
    
    public function login($email, $password)
    {
        $this->db->query('SELECT id, password FROM users WHERE email = :email');
        $this->db->bind(':email', $email);
        $this->db->execute();
        
        $user = $this->db->single();
        
        if(password_verify($password, $user->password))
        {
            $_SESSION['uid'] = $user->id;
            
            return true;
        }
        else
        {
            $_SESSION['error'] = 'We couldn\'t log you in with these credentials.';
            
            return false;
        }
    }
    
    public function logout()
    {
        unset($_SESSION['uid']);
    }
    
    public function name($id)
    {
        $user = $this->find($id);
        
        return $user->name;
    }

    public function address($id)
    {
        $user = $this->find($id);

        return $user->address;
    }

    public function postalCode($id)
    {
        $user = $this->find($id);

        return $user->zipcode;
    }

    public function city($id)
    {
        $user = $this->find($id);

        return $user->city;
    }

    public function email($id)
    {
        $user = $this->find($id);

        return $user->email;
    }
    
    public function is_loggedin()
    {
        if(isset($_SESSION['uid']))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function update()
    {
        $data = $_POST;
        
        $user = $this->find($_SESSION['uid']);
        
        $this->db->query('UPDATE users SET email = :email, address = :address, zipcode = :zipcode, city = :city, country = :country WHERE id = :id');
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':address', $data['address']);
        $this->db->bind(':zipcode', $data['zipcode']);
        $this->db->bind(':city', $data['city']);
        $this->db->bind(':country', $data['country']);
        $this->db->bind(':id', $user->id);
        
        if($this->db->execute())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function updatePassword($old_password, $password_1, $password_2)
    {
        $user = $this->find($_SESSION['uid']);
        
        $this->db->query('SELECT password FROM users WHERE id = :id');
        $this->db->bind(':id', $user->id);
        $this->db->execute();
        
        $user_details = $this->db->single();
        
        if(password_verify($old_password, $user_details->password))
        {
            if($password_1 ==  $password_2)
            {
                $new_password = password_hash($password_1, PASSWORD_BCRYPT);
                
                $this->db->query('UPDATE users SET password = :password WHERE id = :id');
                $this->db->bind(':password', $new_password);
                $this->db->bind(':id', $user->id);
                if($this->db->execute())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}