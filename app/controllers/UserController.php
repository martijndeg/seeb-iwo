<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 18/06/2018
 * Time: 17:06
 */

    namespace app\controllers;
    
    use app\core\Core;
    use app\models\User;
    use app\models\Product;

    class UserController
    {
        public function __construct()
        {
            $this->user = new User();
            $this->product = new Product();
        }
        
        public function register()
        {
            $data['countries'] = json_decode(file_get_contents('https://restcountries.eu/rest/v2/regionalbloc/eu'));
            
            Core::set_page_global_data(['title_prefix' => 'Login or Register']);
            Core::view('register', $data);
        }
        
        public function save()
        {
            $data = $_POST;
            
            Core::set_page_global_data(['title_prefix' => 'Login or Register']);
            
            if($this->user->save($data))
            {
                if($this->user->login($data['email'], $data['password1']))
                {
                    $data['countries'] = json_decode(file_get_contents('https://restcountries.eu/rest/v2/regionalbloc/eu'));
                    $data['current_user'] = $this->user->find($_SESSION['uid']);
    
                    Core::set_page_global_data(['title_prefix' => 'My Account']);
                    Core::view('account', $data);
                }
                else
                {
                    
                    Core::view('register', $data);
                }
            }
            else
            {
                Core::view('register', $data);
            }
        }
        
        public function update()
        {
            $data = $_POST;
            
            if($this->user->update())
            {
                $data['countries'] = json_decode(file_get_contents('https://restcountries.eu/rest/v2/regionalbloc/eu'));
                $data['current_user'] = $this->user->find($_SESSION['uid']);
    
                Core::set_page_global_data(['title_prefix' => 'My Account']);
                Core::view('account', $data);
            }
        }
        
        public function signin()
        {
            $data = $_POST;
            
            if($this->user->login($data['email'], $data['password']))
            {
                header('Location: http://'.$_SERVER['SERVER_NAME']);
            }
            else
            {
                $_SESSION['error'] = 'Something went wrong with your details. Please try again.';
                
                Core::set_page_global_data(['title_prefix' => 'Login or Register']);
                Core::view('register');
            }
        }
        
        public function logout()
        {
            $this->user->logout();
            
            header('Location: http://'.$_SERVER['SERVER_NAME']);
        }
        
        public function account()
        {
            $data['countries'] = json_decode(file_get_contents('https://restcountries.eu/rest/v2/regionalbloc/eu'));
            $data['current_user'] = $this->user->find($_SESSION['uid']);
            
            Core::set_page_global_data(['title_prefix' => 'My Account']);
            Core::view('account', $data);
        }
        
        public function changePassword()
        {
            $data = $_POST;
    
            $data['countries'] = json_decode(file_get_contents('https://restcountries.eu/rest/v2/regionalbloc/eu'));
            $data['current_user'] = $this->user->find($_SESSION['uid']);
            
            $this->user->updatePassword($data['old_password'], $data['password1'], $data['password2']);
            
            Core::set_page_global_data(['title_prefix' => 'My Account']);
            Core::view('account', $data);
        }
    }