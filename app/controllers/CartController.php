<?php

namespace app\controllers;

use app\core\Core;
use app\models\Cart;
use app\models\Product;

Class CartController
{


    public function __construct()
    {
//        $this->model = new Cart();
        $this->product = new Product();
    }
    
    public function index()
    {
        $products = [];
        $data = [];
        
        if(isset($_SESSION['cart']) && count($_SESSION['cart']) > 0)
        {
            $data['subtotal'] = 0;
            
            for($i = 0; $i < count($_SESSION['cart']); $i++)
            {
                $products[$i] = $this->product->find($_SESSION['cart'][$i]['id']);
                $products[$i]->quantity = $_SESSION['cart'][$i]['quantity'];
            }
    
            $data['products'] = $products;
    
            //Calculates the subtotal of products in cart
            foreach($_SESSION['cart'] as $cart_item)
            {
                (int) $data['subtotal'] = $data['subtotal'] + $this->product->find($cart_item['id'])->price * $cart_item['quantity'];
            }
    
            //Calculate shipment costs
            if($data['subtotal'] < 50000 && $data['subtotal'] > 0)
            {
                (int) $data['shipment'] = 1995;
            }
            else
            {
                (int) $data['shipment'] = 0;
            }
        }
        
        Core::set_page_global_data(['title_prefix' => 'My Cart']);
    
        Core::view('cart', $data);
    }
    
    public function add($id)
    {
        $form_data = $_POST;
        
        if(isset($id) && !empty($form_data['quantity']))
        {
            //Puts the products in a session
            $_SESSION['cart'][] = ['id' => (int) $id, 'quantity' => (int) $form_data['quantity']];
        }
    
        header('Location: http://'.$_SERVER['SERVER_NAME'].'/cart');
    }
    
    public function delete($id)
    {
        //deletes the product
        foreach($_SESSION['cart'] as $key => $value)
        {
            if($value['id'] == $id)
            {
                array_splice($_SESSION['cart'], $key, 1);
            }
        }
    
        header('Location: http://'.$_SERVER['SERVER_NAME'].'/cart');
    }
    
    public function update()
    {
        $data = $_POST;

        for($i = 0; $i < count($data['product']); $i++)
        {
            for($j = 0; $j < count($_SESSION['cart']); $j++)
            {
                if(($_SESSION['cart'][$j]['quantity'] !== $data['quantity'][$i]) && ($_SESSION['cart'][$j]['id'] == $data['product'][$i]))
                {
                    if($data['quantity'][$i] == 0)
                    {
                        array_splice($_SESSION['cart'], $i, 1);
                    }
                    else
                    {
                        $_SESSION['cart'][$j]['quantity'] = $data['quantity'][$i];
                    }
                }
            }
        }
    
        header('Location: http://'.$_SERVER['SERVER_NAME'].'/cart');
    }

    public function checkout()
    {
        foreach($_SESSION['cart'] as $item)
        {
            $this->product->sell($item);
        }
        
        unset($_SESSION['cart']);
        
        Core::set_page_global_data(['title_prefix' => 'Thanks for shopping with us']);

        Core::view('shipment');
    }
}