<?php

    namespace app\controllers;
    
    use app\core\Core;
    use app\models\Brand;
    use app\models\Category;
    use app\models\Product;
    
    class ProductController
    {
        public function __construct()
        {
            $this->brand = new Brand();
            $this->product = new Product();
            $this->category = new Category();
        }
    
        public function index()
        {
            $data['products'] = $this->product->getAll('b.name');
            $data['brands'] = $this->brand->getAll('name');
            $data['categories'] = $this->category->getAll('name');
            $data['bestsellers'] = $this->product->getAll('stock');
    
            Core::set_page_global_data(['title_prefix' => 'Our Collection']);
            Core::view('products', $data);
        }
        
        public function show($id)
        {
            $data['product'] = $this->product->find($id);
            $data['related'] = $this->product->related($data['product']->id);
            
            Core::set_page_global_data(['title_prefix' => $data['product']->name]);
            Core::view('product', $data);
        }
        
        public function category($id)
        {
            $data['products'] = $this->product->getAll('b.name', 'ASC', '', 1000, $id);
            $data['brands'] = $this->brand->getAll('name');
            $data['categories'] = $this->category->getAll('name');
            $data['bestsellers'] = $this->product->getAll('stock');
            
            Core::set_page_global_data(['title_prefix' => $this->category->find($id)->name]);
            Core::view('products', $data);
        }

        public function brand($id)
        {
            if(isset($id))
            {
                $data['products'] = $this->product->getAll('name', 'ASC', $id);
                $data['brands'] = $this->brand->getAll('name');
                $data['categories'] = $this->category->getAll('name');
                $data['bestsellers'] = $this->product->getAll('stock', 'ASC', $id);

                Core::set_page_global_data(['title_prefix' => $this->brand->find($id)->name]);
            }
            else
            {
                $data['bestsellers'] = $this->product->getAll('stock');
    
                Core::set_page_global_data(['title_prefix' => $id]);
            }
            
            Core::view('products', $data);
        }
    }