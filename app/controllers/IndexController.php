<?php

    namespace app;

    use app\core\Core;
    use app\models\Product;
    use app\models\IndexModel;

    class IndexController
    {
        private $model;
    
        function __construct($tile)
        {
            /** Loading the corresponding Model class **/
            $this->model = new IndexModel();
        }
        
        public function index()
        {
            $product = new Product();
    
            $data['products'] = $product->getAll('price', 'DESC', null, 8);
    
            $data['deals'] = $product->getAll('name', 'ASC', null, 8);
            
            Core::set_page_global_data(['title_prefix' => 'Home']);
    
            Core::view('main', $data);
        }
    }