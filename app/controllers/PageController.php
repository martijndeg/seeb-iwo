<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 18/06/2018
 * Time: 15:46
 */

namespace app\controllers;

use app\core\Core;
use app\models\Page;

class PageController
{
    public function show($id)
    {
        $page_instance = new Page();
        
        $page = $page_instance->find($id);
        
        $data['page_title'] = $page->title;
        $data['content'] = $page->content;
        $data['image_path'] = $page->image_path;
        
        Core::set_page_global_data(['title_prefix' => $page->title]);
        
        Core::view('page', $data);
    }
    
}