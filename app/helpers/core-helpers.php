<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 17/06/2018
 * Time: 12:32
 */

function display_price($price)
{
    return number_format($price / 100, 2, ',', '.');
}