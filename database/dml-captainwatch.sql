--
-- Database: `lolhan_capwatch`
--

-- --------------------------------------------------------
--
-- Dummy data for table `brands`
--

INSERT INTO `brands` (`id`, `name`) VALUES
(1, 'FOSSIL'),
(2, 'Michaels Kors'),
(3, 'MVMT'),
(4, 'Nixon'),
(5, 'TW Steel');

-- --------------------------------------------------------
--
-- Dummy data for table `categories`
--

INSERT INTO `categories` (`id`, `gender`, `name`) VALUES
(3, 1, 'Steel'),
(4, 1, 'Leather'),
(5, 1, 'Digital'),
(6, 2, 'Steel'),
(7, 2, 'Leather'),
(8, 2, 'Digital');

-- --------------------------------------------------------
--
-- Dummy data for table `genders`
--

INSERT INTO `genders` (`id`, `gender`) VALUES
(1, 'f'),
(2, 'm');

-- --------------------------------------------------------
--
-- Dummy data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `image_path`) VALUES
(1, 'About us', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id molestie tortor. Nulla at lacinia est. Cras fermentum nulla et mi finibus, at iaculis libero posuere. Sed hendrerit a nisl id venenatis.</p><p>Vestibulum sit amet volutpat lorem. Integer vitae purus eu elit cursus pellentesque. Cras at mauris neque. Vivamus dictum quis enim et lobortis. Phasellus egestas quis elit nec lacinia. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p>Sed ultricies metus sit amet ipsum mattis molestie. Donec eu neque sed nunc consectetur fermentum et ut purus. Nullam finibus enim vitae tortor sollicitudin, a fermentum nisi posuere.</p>', 'http://zaplee.com/images/zaplee-phone-woman.jpg');

-- --------------------------------------------------------
--
-- Dummy data for table `products`
--

INSERT INTO `products` (`id`, `brand`, `name`, `description`, `category`, `color`, `price`, `stock`, `image_path`, `discount`) VALUES
(2, 2, 'Bradshaw Rose Gold-Tone', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 3, 'Yellow Gold', 19995, 5, 'https://i.pinimg.com/originals/d8/bd/d3/d8bdd329bd8273db6de32c8ae41506f4.jpg', 0),
(3, 2, 'Parker Rose Gold-Tone Blush', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 3, 'Rose Gold', 13995, 10, 'https://www.richyrewards.com/wp-content/uploads/2015/07/2114762837946.jpg', 0),
(4, 4, 'BASE TIDE PRO 42 MM', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 5, 'Black', 11995, 10, 'https://static.highsnobiety.com/wp-content/uploads/2012/08/nixon-pinstripe-watch-collection-0.jpg', 0),
(5, 2, 'Ritz Two Tone', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 6, 'Silver/Gold', 29995, 7, 'https://bilder.pcwelt.de/3969573_original.jpg', 0),
(6, 3, 'The 40 - Rose Gold Brown', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 7, 'Rose Gold/Brown', 14995, 20, 'http://static2.uk.businessinsider.com/image/599c52eed0302040630d6912-1190-625/save-an-extra-40-at-bonobos-summer-sale--and-more-of-todays-best-deals-from-around-the-web.jpg', 0),
(7, 1, 'Grant Chronograph Brown', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 4, 'Brown', 22495, 5, 'http://www.mundoempresarial.pe/images/reloj-fossil.png', 0),
(8, 3, 'Orion Nova', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 7, 'Yellow Gold/Rose Gold', 19995, 2, 'https://thegadgetflow.com/wp-content/uploads/2014/07/MVMT-watches-inpost1.jpg', 0),
(9, 1, 'Q Founder', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 3, 'Gun Metal', 27995, 4, 'http://www.watch4you.com.ua/public/files/article/38055745_1522252131.jpg', 0),
(10, 1, 'Scarlette Multifunction Rose Gold', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 6, 'Yellow Gold / Emeralds', 15995, 10, 'https://i.pinimg.com/originals/ed/5e/a1/ed5ea15462dd90518c90859bd8c66a8c.jpg', 0),
(11, 2, 'MK3407', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 6, 'Black/Emeralds', 22495, 5, 'https://www.richyrewards.com/wp-content/uploads/2015/07/4814779242635.jpg', 0),
(12, 3, 'Classic Watch', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 4, 'Brown/White', 14995, 3, 'http://gearnova.com/wp-content/uploads/2017/05/MVMT-ALL-BLACK-2-600x300.jpg', 0),
(13, 4, 'Medium Time Teller Watch', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 6, 'Yellow Gold', 20995, 7, 'https://static.highsnobiety.com/wp-content/uploads/2013/04/nixon-48-20-chrono-ltd-00.jpg', 0),
(14, 5, 'CB202 Canteen', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 3, 'Gray', 17495, 3, 'https://helenkirchhofer.ch/media/wysiwyg/impudicus/brands/tw_steel/tw-steel-herrenuhren.jpg', 0),
(15, 1, 'Nate', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 3, 'Black', 16995, 14, 'https://cdn.bestadvisers.co.uk/reviews/e7/1c/e71c7e31503dac156424486b6fd02208.jpg', 0),
(16, 2, 'Dylan MK8295', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 4, 'Yellow Gold', 26995, 6, 'https://www.bataviastad.nl/wp-content/uploads/2018/01/michaelkors-smartwatchrose.jpg', 0),
(17, 5, 'Canteen CS10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 4, 'Black', 17495, 10, 'https://www.succes.network/images/bedrijven/12_D.jpg', 0),
(18, 4, 'Unit SS Rose Gold Digital', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 8, 'Yellow Gold', 19995, 2, 'https://static.highsnobiety.com/wp-content/uploads/2013/06/nixon-51-30-chrono-leather-highsnobiety-0.jpg', 0),
(19, 3, 'La Boheme Mesh Black', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 4, 'Black', 11995, 8, 'https://thegadgetflow.com/wp-content/uploads/2014/12/giftguide-forher-inpost6.jpg', 0),
(20, 1, 'Townsman Automatic Dark Brown', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 4, 'Dark Brown', 18995, 3, 'https://pbs.twimg.com/media/Cp_UNOTWcAE8XCK.jpg', 0),
(21, 2, '3737 Portia', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam interdum, nisi id consequat porta, lacus dolor commodo mi, vitae auctor felis massa eget mauris. Morbi eu ornare libero. Praesent lacinia laoreet aliquam. In sagittis lacus vel scelerisque eui', 3, 'Yellow Gold', 10995, 7, NULL, 0);

-- --------------------------------------------------------
--
-- Dummy data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `address`, `zipcode`, `city`, `country`) VALUES
(1, 'email@example.org', 'John Doe', '$2y$10$8k2jTNhBNX5wurAOob5nAO1qxOxo9Y89zwlRlSCSwJMXE0Th0mE9K', 'Highstreet 3', '123456', 'Fairy Town', 'GBR');